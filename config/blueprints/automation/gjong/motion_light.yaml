blueprint:
  name: Motion based light trigger
  description: Light switching based upon a (binary) motion sensor and the luminosity in the location.
  domain: automation
  input:
    motion_trigger:
      name: Triggering Sensor - Binary Sensor
      description: The Sensor/s that turns the lights ON and OFF. The trigger can
        be any [Binary Sensors](https://www.home-assistant.io/integrations/binary_sensor/)
        you like.
      selector:
        entity:
          domain: binary_sensor
          multiple: true
    include_sun:
      name: Use The Sun Option (Optional)
      description: The sun can be used to automatically adjust brightness.
      default: none
      selector:
        select:
          options:
            - label: Enable the sun option
              value: sun_enabled
            - label: Disable the sun option
              value: sun_disabled
          custom_value: false
          multiple: false
    sun_elevation:
      name: Sun Elevation
      description: This is the angle between the sun and the horizon. Negative values
        mean the sun is BELOW the horizon. Guide is -1.5 (dusk) and -4.0 (dawn).
      default: -1.5
      selector:
        number:
          min: -10.0
          max: 5.0
          step: 0.5
          unit_of_measurement: degrees
          mode: slider
    include_ambient:
      name: Use The Ambient Option (Optional)
      description: Select enable or disable.
      default: none
      selector:
        select:
          options:
            - label: Enable the ambient option
              value: ambient_enabled
            - label: Disable the ambient option
              value: ambient_disabled
          custom_value: false
          multiple: false
    ambient_light_sensor:
      name: Ambient Light Sensor
      description: This is used for adding a condition to only work when it is dark
        or below the Ambient Light LUX Value. If you have selected an entity and you
        would like to clear this selection see our [FAQ](https://community.home-assistant.io/t/sensor-light-motion-sensor-sun-elevation-lux-value-scenes-time/481048/6)
      default: []
      selector:
        entity:
          domain: sensor
          device_class: illuminance
          multiple: false
    ambient_light_options:
      name: Ambient Light Sensor - Site Conditions
      description: In some cases when your lights turn ON your ambient light sensor
        is affected increasing its LUX value. This can cause the lights to go OFF
        prematurely. Please select an option that best suits your installation.
      default: none
      selector:
        select:
          options:
            - label: YES - My Ambient Light Sensor is affected by the Lights
              value: ambient_light_option_enabled
            - label: NO - My Ambient Light Sensor is not affected by the Lights
              value: ambient_light_option_disabled
          custom_value: false
          multiple: false
    ambient_light_value:
      name: Ambient Light LUX Value
      description: Set the Ambient Light Value if you have selected Ambient Light
        Sensor. Guide is 20 lux (dusk) and 80 lux (dawn).
      default: 20
      selector:
        number:
          min: 0.0
          max: 500.0
          step: 10.0
          unit_of_measurement: LUX
          mode: slider
    light_switch:
      name: Lights - Switches - Scenes
      description:  The lights that get turned on by the trigger sensor/s. You can
        also add switches and scenes. If adding a scene please read "Scenes To Turn
        OFF" below.
      selector:
        target:
          entity:
            domain:
              - light
    include_time:
      name: Use The Time Option (Optional)
      description: Use the "Start Time" and the "End Time" values to only run between the time periods.
      default: none
      selector:
        select:
          options:
            - label: Enable the time option
              value: "time_enabled"
            - label: Disable the time option
              value: "time_disabled"
    after_time:
      name: Start Time
      description: After what time should it activate.
      default: 00:00:00
      selector:
        time:
    before_time:
      name: End Time
      description: Before what time should it activate.
      default: 00:00:00
      selector:
        time:
mode: restart
max_exceeded: silent
variables:
  motion_trigger: !input motion_trigger
  light_switch: !input light_switch
  include_sun: !input include_sun
  sun_elevation: !input sun_elevation
  include_ambient: !input include_ambient
  ambient_light_sensor: !input ambient_light_sensor
  ambient_light_options: !input ambient_light_options
  ambient_light_value: !input ambient_light_value
  include_time: !input include_time
  after_time: !input after_time
  before_time: !input before_time
trigger:
  - platform: state
    id: t1
    entity_id: !input motion_trigger
    from: 'off'
    to: 'on'
  - platform: numeric_state
    id: t2
    entity_id: sun.sun
    attribute: elevation
    below: !input sun_elevation
  - platform: numeric_state
    id: t3
    entity_id: !input ambient_light_sensor
    below: !input ambient_light_value
  - platform: time
    id: "t4"
    at: !input after_time
condition:
  - condition: or
    conditions:
      - condition: and
        conditions:
          - condition: state
            entity_id: !input motion_trigger
            match: any
            state: 'on'
      - condition: and
        conditions:
          - condition: state
            entity_id: !input motion_trigger
            state: 'on'
          - condition: trigger
            id: t2
      - condition: and
        conditions:
          - condition: state
            entity_id: !input motion_trigger
            state: 'on'
          - condition: trigger
            id: t3
      - condition: and
        conditions:
          - condition: state
            entity_id: !input motion_trigger
            state: 'on'
          - condition: trigger
            id: 't4'
  - condition: or
    conditions:
      - '{{ include_ambient == ''none'' }}'
      - '{{ include_ambient == ''ambient_disabled'' }}'
      - '{{ ambient_light_sensor == ''none'' }}'
      - '{{ ambient_light_value == ''none'' }}'
      - '{{ (include_ambient == ''ambient_enabled'') and (states[ambient_light_sensor].state
    | int < ambient_light_value | int) }}'
      - '{{ (ambient_light_options == ''ambient_light_option_enabled'') and (expand(light_switch.entity_id)
    | selectattr(''state'', ''=='', ''on'') | list | count > 0) }}'
  - condition: or
    conditions:
      - "{{ include_time == 'none' }}"
      - "{{ include_time == 'time_disabled' }}"
      - condition: and
        conditions:
          - condition: time
            after: !input after_time
            before: !input before_time
          -  "{{ include_time == 'time_enabled' }}"
action:
  - alias: Turn on the light
    service: light.turn_on
    data:
      brightness_pct: >
        {% if (include_sun == 'sun_enabled') and (state_attr('sun.sun','elevation') <= sun_elevation | float(90)) %}
        25
        {% else %}
        100
        {% endif %}
    target: !input light_switch
  - alias: Wait until motion sensor is off
    wait_for_trigger:
      platform: state
      entity_id: !input motion_trigger
      from: 'on'
      to: 'off'
  - alias: Turn off the lights
    service: light.turn_off
    target: !input light_switch
