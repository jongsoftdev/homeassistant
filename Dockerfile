# This image is used to validate the configuration of Home Assistant before deployment to the actual server
FROM homeassistant/home-assistant

RUN echo "" > /config/home-assistant.log \
    && wget -O - https://get.hacs.xyz | bash -

# Since this changes more often then the layers above do this last
ADD .stubs/secrets.yaml /config/secrets.yaml
ADD config /config

CMD python -m homeassistant --config /config/ --script check_config
