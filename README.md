# Gjong Home Assistant Configuration

![maintained](https://img.shields.io/maintenance/yes/2022)
![license](https://img.shields.io/badge/license-MIT-green)

![Config pass](https://img.shields.io/circleci/build/bitbucket/jongsoftdev/homeassistant)
![Open issues](https://img.shields.io/bitbucket/issues-raw/jongsoftdev/homeassistant)

## About

This repository contains the Home Assistant configuration as I use it for my house. It contains a wide variate of
components as well as several custom ones. And a styled interface using the Lovelace interface technology,

## Configuration structure

### Overall setup

The main `configuration.yaml` file is as small as possible. The only thing it includes is the Home Assistant package
import. This tells it to import everything contained in the `integrations` directory at the root level of the
configuration. For integrations that have multiple entities (eg: sensor, template_sensor) a separate directory exists in
the `integrations` directory with the name of the type (eg: `integrations\sensor`).

For lovelace the UI editing has been disabled, and it is maintained in the `ui-lovelace.yaml` file which imports the
various views from `lovelace/views`, cards from `lovelace/cards` and resources from `lovelace/resources`.

This leads to the following directory structure:

```
  - config
   - blueprints
     - automations
       - gjong
         - ${first_automation}.yaml
   - integrations
     - ${integration_name}
       - ${first_entry}.yaml
       - ${second_entry}.yaml
     - ${integration_name}.yaml
   - lovelace
     - cards
       - ${popup_type}.yaml
     - resources
       - ${resource_name}.yaml
     - views
       - ${view}.yaml
```

### Setup of the automation scripts [deprecated]

**====== Important note ======**

Since Home Assistant decided to remove most of the yaml configuration for integrations I no longer use the automation
integration in Home Assistant. The new approach generates semi random entity_id for each integration. This makes
configuration checking impossible and automation writing difficult when assuming a fresh installation every update.

## Hardware / Software list

The home automation installation in the house consists out of a wide variety of hardware and software components. Below
is the setup that is currently in use.

### Server hardware

The following hardware is used to host and control the entire home automation system.

| Brand               | Type                                                                                                                            | Amount | Reason                                         |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------|--------|------------------------------------------------|
| Intel               | NUC8i3BEK                                                                                                                       | 1      | Hosting the kubernetes cluster                 |
| Dresden Elektronikz | [Conbee II](https://phoscon.de/en/conbee2)                                                                                      | 1      | For running Zigbee network                     |
| Dresden Elektronikz | [Raspbee](https://phoscon.de/en/raspbee)                                                                                        | 1      | Demoted to backup solution for Zigbee network. |
| Raspberry           | Pi3                                                                                                                             | 1      | Has the Raspbee attached                       |
| M5Stack             | [ATOM Lite ESP32 IoT Development Kit](https://shop.m5stack.com/products/atom-lite-esp32-development-kit?variant=32259605200986) | 3      | Used as BT proxies throughout the home.        |

### Camera's

In the home automation several cameras are used to secure the household.

| Brand  | Type          | Amount |
|--------|---------------|--------|
| Ring   | Spotlight Cam | 2      |
| Ring   | Stick Up Cam  | 1      |
| Ring   | Doorbell 3    | 1      |
| Xiaomi | deFang        | 2      |

### Switches

| Brand     | Type                                                                                                                                                                                                         | Amount |
|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| Innr      | SP120                                                                                                                                                                                                        | 4      |
| Innr      | [SP220](https://www.innr.com/en/product/smart-plug-zigbee30/)                                                                                                                                                | 1      |
| BlitzWolf | [BW-SHP-13](https://m.blitzwolf.com/BlitzWolf-BW-SHP13-ZigBee-3.0-Smart-Socket-EU-16A-3680W-with-ZigBee-3.0-Linkage,-High-load-Rating,-Electricity-Monitoring,-Timing-Function-and-Voice-Control-p-518.html) | 8      |
| TP        | [HS-110](https://www.tp-link.com/en/home-networking/smart-plug/hs110/)                                                                                                                                       | 1      |
| Xiaomi    | Aqara Switches                                                                                                                                                                                               | 2      |
| Ikea      | [Remote](https://www.ikea.com/nl/nl/p/tradfri-afstandsbediening-30443124/)                                                                                                                                   | 2      |
| Signify   | [Dimmer](https://www.philips.nl/c-p/8718696743157/hue-dimmer)                                                                                                                                                | 1      |

### Lights

| Brand   | Type                                                                                                              | Amount |
|---------|-------------------------------------------------------------------------------------------------------------------|--------|
| Ikea    | [Tradfri GU10](https://www.ikea.com/nl/nl/p/tradfri-led-lamp-gu10-400-lumen-draadloos-dimbaar-warm-wit-60420041/) | 11     |
| Trust   | [ZLED-RGB9](https://www.trust.com/nl/product/71145-zigbee-rgb-tunable-led-bulb-zled-rgb9)                         | 1      |
| Signify | [HUE GU10](https://www.philips-hue.com/nl-nl/p/hue-white-ambiance-1-pack-gu10/8719514339903)                      | 3      |
| Signify | [HUE E27](https://www.philips-hue.com/nl-nl/p/hue-white-2-pack-e27/8719514319028)                                 | 2      |
| Innr    | RB-145 (E14)                                                                                                      | 3      |
| Innr    | RB 165 (E27)                                                                                                      | 3      |

### Sensors

| Brand   | Type                                                                                                   | Amount |
|---------|--------------------------------------------------------------------------------------------------------|--------|
| Trust   | Door sensor [ZCTS-808](https://www.trust.com/nl/product/71169-zigbee-wireless-contact-sensor-zcts-808) | 1      |
| Trust   | Smoke detector [ZSDR-850](https://www.trust.com/en/product/71197-zigbee-smoke-detector-zsdr-850)       | 1      |
| Xiaomi  | Aqara Contact sensor                                                                                   | 5      |
| Xiaomi  | Aqara Temperature sensor                                                                               | 4      |
| Xiaomi  | Aqara motion sensor                                                                                    | 1      |
| Xiaomi  | miFlora                                                                                                | 4      |
| Signify | [HUE motion sensor](https://www2.meethue.com/nl-nl/p/hue-bewegingssensor/8718696743171)                | 3      |
| Youless | [LS120](https://www.youless.nl/)                                                                       | 1      |
| Tado    | V3 Thermostat                                                                                          | 1      |

### Supporting software

Since not everything plays nicely with docker or kubernetes some software packages are hosted directly on the NUC
itself. These include:

* [plantgateway](https://github.com/ChristianKuehnel/plantgateway), tool to read Xiaomi MiFlora sensors
* [BTE monitor](https://github.com/andrewjfreyer/monitor), tool to use presence detection using BT-LE in various
  locations throughout the house

## Used custom components

In the repository there are several git submodules linking the used custom components into the configuration build. The
following custom components are currently in use:

* [Afvalwijzer sensor](https://github.com/xirixiz/Home-Assistant-Sensor-Afvalwijzer)
* [Govee](https://github.com/LaggAt/hacs-govee)
* [OpenPlantbook](https://github.com/Olen/home-assistant-openplantbook)
* [Home Assistant Plant](https://github.com/Olen/homeassistant-plant)
* [Github Release monitor](https://bitbucket.org/jongsoftdev/github-release-monitor/)

## Deployment

Home Assistant is currently running inside a Kubernetes cluster using microk8s implementation. The container is running
in an isolated namespace with all dependant containers.

For my installation this means the following deployments exists in the `home-automation` namespace of Kubernetes:

* Home Assistant (image: [homeassistant/home-assistant](https://hub.docker.com/r/homeassistant/home-assistant))
* InfluxDB (image: [influxdb](https://hub.docker.com/_/influxdb))
* MariaDB (image: [mariadb](https://hub.docker.com/_/mariadb))
* Node-Red (image: [nodered/node-red](https://hub.docker.com/r/nodered/node-red))
* Grafana (image: [grafana/grafana](https://hub.docker.com/r/grafana/grafana))

This is combined with a private and public Ingress to expose the various containers to the outside world.

## License

MIT License

Copyright (c) 2018-2022 Gerben Jongerius

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
